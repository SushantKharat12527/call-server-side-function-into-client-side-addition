﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Addition.aspx.cs" Inherits="Sol_ServersidefunctionIntoClientAddition.Addition" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

    <script type="text/javascript">

        function pageLoad() {
            $(document).ready(function () {
                alert("Dom is ready");

                $(btnAdd).click(function () {
                    
                    var result =parseInt(document.getElementById('txtNum1').value) + parseInt(document.getElementById('txtNum2').value);
                    //alert(document.getElementById('txtNum1').value);
                    $("#lblMessage").text(result);
                });
                    
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <asp:ScriptManager ID="scriptManger" runat="server" EnablePageMethods="true">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                 <table>
                            <tr>
                                <td>
                                    <input type="text" id="txtNum1" placeholder="Number 1" />
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <input type="text" id="txtNum2"  placeholder="Number2" />
                                </td>
                            </tr>
                             <tr>
                                <td>
                                       <input type="button" id="btnAdd" value="SignIn" />
                                </td>
                            </tr>
                             <tr>
                                <td>
                                       <div id="divResultPanel" >
                                            <span id="lblMessage"></span>
                                        </div>
                                </td>
                            </tr>
                        </table>
                    
        </ContentTemplate>
                 </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
